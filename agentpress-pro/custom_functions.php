<?php

add_action( 'wp_enqueue_scripts', 'agentpress_custom_styles' );
function agentpress_custom_styles() {
   wp_enqueue_style( 'agentpress-custom-css', get_stylesheet_directory_uri() . "/custom_style.css", array(), "1.0", "all" );
}