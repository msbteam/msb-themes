<?php

add_action( 'wp_enqueue_scripts', 'streamline_pro_custom_styles' );
function streamline_pro_custom_styles() {
   wp_enqueue_style( 'streamline-pro-custom-css', get_stylesheet_directory_uri() . "/custom_style.css", array(), "1.0", "all" );
}