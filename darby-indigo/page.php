<?php

remove_action('genesis_post_title', 'genesis_do_post_title');

add_action('genesis_post_title', 'genesis_do_post_title_tm');
/**
 * Post Title
 */
function genesis_do_post_title_tm() {

	$title = get_the_title();

	if ( strlen( $title ) == 0 )
		return;

	if ( is_singular() ) {
		$title = sprintf( '<h1 class="entry-title"><span class="corner-left"></span>%s</h1>', apply_filters( 'genesis_post_title_text', $title ) );
	} else {
		$title = sprintf( '<h2 class="entry-title"><a href="%s" title="%s" rel="bookmark">%s</a></h2>', get_permalink(), the_title_attribute('echo=0'), apply_filters( 'genesis_post_title_text', $title ) );
	}

	echo apply_filters('genesis_post_title_output', $title) . "\n";

}

genesis();