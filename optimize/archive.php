<?php get_header(); ?>

        <div id="featured">
            <div id="page-title" class="col-full">
				<?php if (is_category()) { ?>
                <span class="archive_header"><span class="fl cat"><?php _e('Archive', 'woothemes'); ?> &raquo; <?php echo single_cat_title(); ?></span> </span>        
            
                <?php } elseif (is_day()) { ?>
                <span class="archive_header"><?php _e('Archive', 'woothemes'); ?> &raquo; <?php the_time(get_option('date_format')); ?></span>
    
                <?php } elseif (is_month()) { ?>
                <span class="archive_header"><?php _e('Archive', 'woothemes'); ?> &raquo; <?php the_time('F, Y'); ?></span>
    
                <?php } elseif (is_year()) { ?>
                <span class="archive_header"><?php _e('Archive', 'woothemes'); ?> &raquo; <?php the_time('Y'); ?></span>
    
                <?php } elseif (is_author()) { ?>
                <span class="archive_header"><?php _e('Archive by Author', 'woothemes'); ?></span>
    
                <?php } elseif (is_tag()) { ?>
                <span class="archive_header"><?php _e('Tag Archives:', 'woothemes'); ?> <?php echo single_tag_title('', true); ?></span>
                
                <?php } ?>
            </div>
        </div>

        <div id="breadcrumb">
        	<div class="col-full">
                <div class="fl"><?php if ( function_exists( "yoast_breadcrumb" ) ) yoast_breadcrumb('', ''); ?></div>
                <a class="subscribe fr" href="<?php if ( get_option('woo_feedburner_url') <> "" ) { echo get_option('woo_feedburner_url'); } else { echo get_bloginfo_rss('rss2_url'); } ?>">
                    <img src="<?php bloginfo('template_directory'); ?>/images/ico-rss.png" alt="Subscribe" class="rss" />
                </a>        

                <div class="<?php if ( function_exists( "yoast_breadcrumb" ) ) echo 'fr'; else echo 'fl'; ?>">
					<span class="fr catrss"><a class="subscribe fr" href="<?php if ( get_option('woo_feedburner_url') <> "" ) { echo get_option('woo_feedburner_url'); } else { echo get_bloginfo_rss('rss2_url'); } ?>"><?php _e('RSS feed for this section', 'woothemes'); ?></a></span>
                </div>
			</div>
        </div>              
    
	</div><!-- /#top -->
       
    <div id="content">
    <div class="col-full">
    
		<div id="main" class="col-left">
            
            <?php if (have_posts()) : $count = 0; ?>
            <?php while (have_posts()) : the_post(); $count++; ?>
                <!-- Post Starts -->
                <div class="post">

                    <h2 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                    
                    <p class="post-meta">
                    	<?php _e('Posted on', 'woothemes') ?> <?php the_time(get_option('date_format')); ?>
                    	<?php _e('by', 'woothemes') ?>  <?php the_author_posts_link(); ?>
                    	<?php _e('in', 'woothemes') ?> <?php the_category(', ') ?>
                    </p>
                    
                    <div class="entry">
	                    <?php if ( get_option('woo_excerpt') == "true" ) the_excerpt(); else the_content(); ?>                        
	                </div><!-- /.entry -->
                    
                   	<span class="comments"><?php comments_popup_link(__('0 Comments', 'woothemes'), __('1 Comment', 'woothemes'), __('% Comments', 'woothemes')); ?></span>
                    

                </div><!-- /.post -->
                                                    
			<?php endwhile; else: ?>
				<div class="post">
                	<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
                </div><!-- /.post -->
            <?php endif; ?>  
        
                <div class="more_entries">
                    <?php if (function_exists('wp_pagenavi')) wp_pagenavi(); else { ?>
                    <div class="fl"><?php previous_posts_link(__('&laquo; Newer Entries ', 'woothemes')) ?></div>
                    <div class="fr"><?php next_posts_link(__(' Older Entries &raquo;', 'woothemes')) ?></div>
                    <br class="fix" />
                    <?php } ?> 
                </div>		
                
		</div><!-- /#main -->

        <?php get_sidebar(); ?>

    </div><!-- /.col-full -->
    </div><!-- /#content -->

<?php get_footer(); ?>