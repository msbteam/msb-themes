<?php
if ( ! is_admin() ) { add_action( 'wp_print_scripts', 'woothemes_add_javascript' ); }

function woothemes_add_javascript() {
	$template_directory = get_template_directory_uri();
	   
	wp_enqueue_script( 'superfish', $template_directory . '/includes/js/superfish.js', array( 'jquery' ) );
	wp_enqueue_script( 'innerfade', $template_directory . '/includes/js/innerfade.js', array( 'jquery' ) );
	wp_enqueue_script( 'prettyPhoto', $template_directory . '/includes/js/prettyPhoto.js', array( 'jquery' ) );
	wp_enqueue_script( 'general', $template_directory . '/includes/js/scripts.js', array( 'jquery' ) );
	
	// Load the JavaScript for the slides and testimonals on the homepage.
		
		if ( is_home() ) {
			
			// Load the custom innerfade settings only if necessary.
			
			$can_fade = true;
			$timeout = 6000;
			
			$can_fade_db = get_option( 'woo_testimonials_autofade' );
			$timeout_db = get_option( 'woo_testimonials_interval' ) * 1000;
			
			if ( $can_fade_db == 'true' ) { $can_fade = $can_fade_db; }
			if ( $timeout != '' ) { $timeout = intval( $timeout_db ); }
		
			// Allow our JavaScript file (the general one) to see our fader setup data.
			$data = array(
						'timeout' => $timeout, 
						'can_fade' => $can_fade
						);
			
			wp_localize_script( 'general', 'woo_innerfade_settings', $data );
			wp_enqueue_script( 'slides', $template_directory . '/includes/js/slides.min.jquery.js', array( 'jquery' ) );
		}
}
?>