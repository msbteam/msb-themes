<?php

add_action( 'wp_enqueue_scripts', 'going_green_custom_styles' );
function going_green_custom_styles() {
   wp_enqueue_style( 'going-green-custom-css', get_stylesheet_directory_uri() . "/custom_style.css", array(), "1.0", "all" );
}