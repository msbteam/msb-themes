<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); 
?>


	<?php genesis_before_content(); ?>

	<main class="content">


	<?php 
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post(); ?>
			<header class="entry-header">
				<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
			</header>
			<p class="entry-meta"><?php the_time('F jS, Y'); ?> by <?php the_author_posts_link(); ?></p>
		<?php the_post_thumbnail(); ?>
			<div class="entry-content">
				<?php echo bwp_trim_excerpt($text) ?>
				<br />
				<br />
				<a class="read-more" href="<?php the_permalink(); ?>">Read More</a>
			</div>

			<footer class="entry-footer"></footer>
		<? } // end while ?>
	<? } // end if ?>
	
	
	</main><!-- end #content -->

	<aside class="sidebar sidebar-primary widget-area">
	</aside>

<?php genesis_after_content(); ?>

<?php genesis_after_content_sidebar_wrap(); ?>


<?php get_footer(); ?>